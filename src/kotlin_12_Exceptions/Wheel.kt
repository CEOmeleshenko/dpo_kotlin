package kotlin_12_Exceptions

class Wheel {
    private var currentPressure: Double = 1.0

    fun pumpUpTo(value: Double) {
        if (value in 0.0..10.0) {
            currentPressure = value
            print("При накачке $value атмосфер процедура удалась. ")
        }
        else {
            print("При накачке $value атмосфер процедура не удалась.\n")
            throw IncorrectPressure()
        }
    }

    fun checkLevel() {
        if (currentPressure < 1.6) {
            print("Эксплуатация невозможна — давление ниже нормального.\n")
            throw TooLowPressure()
        }
        else if (currentPressure > 2.5) {
            print("Эксплуатация невозможна — давление превышает нормальное.\n")
            throw TooHighPressure()
        }
        else print("Эксплуатация возможна.\n")
    }
}