package kotlin_12_Exceptions

fun main() {
    val wheel = Wheel()

    try {
        wheel.pumpUpTo(2.0)
        wheel.checkLevel()
    }
    catch(_: Throwable) {  }

    try {
        wheel.pumpUpTo(-5.0)
        wheel.checkLevel()
    }
    catch(_: Throwable) {  }

    try {
        wheel.pumpUpTo(5.0)
        wheel.checkLevel()
    }
    catch(_: Throwable) {  }
}