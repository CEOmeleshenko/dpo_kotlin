package kotlin_03_Variables_types

fun main() {
    val firstName: String = "Andrey";
    val lastName: String = "Meleshenko";
    var height: Double = 185.00;
    val weight: Float = 66f;
    var isChild = height < 150 || weight < 40;

    var info = "Name: $firstName $lastName\nweight: $weight, height: $height\nIs a child: $isChild\n";
    println(info);

    height = 140.00;
    isChild = height < 150 || weight < 40;
    info = "Name: $firstName $lastName\nweight: $weight, height: $height\nIs a child: $isChild\n";
    println(info);
}