package kotlin_10_OOP3

class CurrencyConverterToEUR: CurrencyConverter {
    override var currencyCode: String = "EUR"

    override fun convertRub(amount: Double): Double {
        val result = amount / 65.15
        println("$amount RUB = $result $currencyCode")
        return result
    }
}