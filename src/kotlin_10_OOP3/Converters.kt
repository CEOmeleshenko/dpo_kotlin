package kotlin_10_OOP3

object Converters {
    fun get(currencyCode: String): CurrencyConverter {
        return when (currencyCode) {
            "USD" -> CurrencyConverterToUSD()
            "EUR" -> CurrencyConverterToEUR()
            "BTC" -> CurrencyConverterToBTC()
            else -> return object: CurrencyConverter {
                override var currencyCode: String = ""

                override fun convertRub(amount: Double): Double {
                    println("Неизвестная валюта")
                    return amount
                }
            }
        }
    }
}