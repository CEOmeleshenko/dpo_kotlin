package kotlin_10_OOP3

class CurrencyConverterToBTC: CurrencyConverter {
    override var currencyCode: String = "BTC"

    override fun convertRub(amount: Double): Double {
        val result = (amount / 1_086_487.58)
        println("$amount RUB = $result $currencyCode")
        return result
    }
}