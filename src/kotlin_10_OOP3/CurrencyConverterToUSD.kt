package kotlin_10_OOP3

class CurrencyConverterToUSD: CurrencyConverter {
    override var currencyCode: String = "USD"

    override fun convertRub(amount: Double): Double {
        val result = (amount / 62.20)
        println("$amount RUB = $result $currencyCode")
        return result
    }
}