package kotlin_10_OOP3

fun main() {
    val amountToConvert = 62000.0

    Converters.get("USD").convertRub(amountToConvert)
    Converters.get("EUR").convertRub(amountToConvert)
    Converters.get("BTC").convertRub(amountToConvert)
    Converters.get("GBP").convertRub(amountToConvert)

}