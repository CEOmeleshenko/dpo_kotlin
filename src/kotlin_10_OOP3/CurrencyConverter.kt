package kotlin_10_OOP3

interface CurrencyConverter {
    var currencyCode: String
    fun convertRub(amount: Double): Double
}