package kotlin_13_Coroutines

import kotlinx.coroutines.*
import java.math.BigInteger


fun main() = runBlocking {
    val fib1 = async {
        try {
            withTimeout(3000) { Fibonacci.take(4000) }
        } catch (ex: TimeoutCancellationException) {
            return@async "Превышено время выполнения"
        }
    }
    val fib2 = async {
        try {
            withTimeout(3000) { Fibonacci.take(3266632) }
        } catch (ex: TimeoutCancellationException) {
            return@async "Превышено время выполнения"
        }
    }
    val fib3 = async {
        try {
            withTimeout(3000) { Fibonacci.take(1) }
        } catch (ex: TimeoutCancellationException) {
            return@async "Превышено время выполнения"
        }
    }
    while (fib1.isActive || fib2.isActive || fib3.isActive) {
        print('.')
        delay(40)
    }
    println()
    println("Поток 1: ${fib1.await()}")
    println("Поток 2: ${fib2.await()}")
    println("Поток 3: ${fib3.await()}")
}

object Fibonacci {
    suspend fun take(num: Int): BigInteger {
        var prev: BigInteger = BigInteger.ZERO
        var next: BigInteger = BigInteger.ONE
        for (i in 0..num) {
            next += prev
            prev = next - prev
            yield()
        }
        return prev
    }
}