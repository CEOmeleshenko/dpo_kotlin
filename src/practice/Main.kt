package practice

fun main() {
    print("Сколько номеров нужно ввести: ")
    val num: Int = readln().toInt()
    var listOfPhoneNumbers = getListOfPhoneNumbers(num)

    listOfPhoneNumbers = listOfPhoneNumbers.filter { it.length >= 2 && it.substring(0, 2) == "+7" }.toMutableList()
    println("Количество уникальных номеров: ${ listOfPhoneNumbers.toMutableSet().size }")
    println("Сумму длин всех номеров телефонов: ${listOfPhoneNumbers.sumOf { it.length }}")

    val phonebook = fillPhonebook(listOfPhoneNumbers)
    phonebook.forEach { (phone, name) -> println("Человек: $name. Номер телефона: $phone") }
}


fun getListOfPhoneNumbers(number: Int) : MutableList<String> {
    val listOfPhoneNumbers = mutableListOf<String>()
    for (i in 0 until number) {
        print("${i + 1} -> ")
        listOfPhoneNumbers.add(readln())
    }
    return listOfPhoneNumbers
}

fun fillPhonebook(list: MutableList<String>): MutableMap<String, String> {
    val result = mutableMapOf<String, String>()
    println("Заполните телефонную книгу: <номер> - <имя>")
    for (phone in list) {
        print("<$phone> - ")
        result[phone] = readln()
    }
    return result
}

