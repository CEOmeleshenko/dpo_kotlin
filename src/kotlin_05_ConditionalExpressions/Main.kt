package kotlin_05_ConditionalExpressions

fun main() {
    println("Fibonacci Numbers")
    var num = numSearch()
    println("The $num of fibonacci: " + fibCalc(num))
}

fun numSearch(): Int {
    while (true) {
        print("Enter a number bigger than 0 -> ")
        var n: Int = readLine()?.toIntOrNull() ?: continue
        if (n > 0) {
            return n
        } else continue
    }
}

fun fibCalc(number: Int): Int {
    return if (number in 1..2) 1
    else {
        var fib1 = 1
        var fib2 = 1
        for (i in 3..number) {
            var temp = fib1 + fib2
            fib1 = fib2
            fib2 = temp
        }
        fib2
    }
}
