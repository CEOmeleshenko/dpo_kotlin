package kotlin_09_OOP2

open class DebitCard : BankCard() {
    override var balance: Int = 0

    override fun deposit(amount: Int) {
        balance += amount
        println("Баланс пополнен на $amount")
    }

    override fun pay(amount: Int) {
        if (amount <= balance) balance -= amount
        else println("Недостаточно средств")
    }

    override fun getBalance() {
        println("Баланс дебетовой карты: $balance")
    }

    override fun getAll() {
        println(
            """
            -- Вид карты: дебетовая
            | Баланс: $balance
            """.trimIndent()
        )
    }
}