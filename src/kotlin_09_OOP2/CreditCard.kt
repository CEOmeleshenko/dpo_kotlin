package kotlin_09_OOP2

open class CreditCard : BankCard() {
    override var balance: Int = 0
    var creditLimit: Int = 10000
    protected var creditBalance: Int = creditLimit

    override fun deposit(amount: Int) {
        if (creditBalance == creditLimit) balance += amount
        else {
            if (amount <= creditLimit - creditBalance) creditBalance += amount
            else {
                balance += amount - (creditLimit - creditBalance)
                creditBalance = creditLimit
            }
        }
    }

    override fun pay(amount: Int) {
        if (amount <= balance) balance -= amount
        else {
            if (amount <= creditLimit) {
                creditBalance -= amount - balance
                balance = 0
            }
            else {
                println("Операция отменена. Выход за лимит")
                return
            }
        }
    }

    override fun getBalance() {
        println("Доступные средства: ${balance + creditBalance}")
    }

    override fun getAll() {
        println(
            """
            -- Вид карты: кредитная
            | Кредитный лимит: $creditLimit
            | Баланс: ${balance + creditBalance}
            |     Собственный: $balance
            |     Кредитный: $creditBalance
            """.trimIndent()
        )
    }

}