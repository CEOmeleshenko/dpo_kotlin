package kotlin_09_OOP2

fun main(){
    val card1 = MasterCardDebit()
    val card2 = ProCardDebit()
    val card3 = MasterCardCredit()

    card1.deposit(10000)
    card2.deposit(6000)
    card3.deposit(12000)

    card3.getAll()

    card1.pay(4600)
    card2.pay(6000)

    card1.getAll()
    card2.getAll()
}