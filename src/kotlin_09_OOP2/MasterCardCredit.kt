package kotlin_09_OOP2

class MasterCardCredit: CreditCard() {
    private var capital: Double = 0.0

    override fun deposit(amount: Int) {
        if (creditBalance >= creditLimit) {
            capital += amount * 0.00005
            println("Накопление в размере ${amount * 0.00005}")
        }
        super.deposit(amount)
    }

    override fun getAll() {
        super.getAll()
        println("|     Накопление: $capital")
    }
}