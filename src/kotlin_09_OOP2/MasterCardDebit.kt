package kotlin_09_OOP2

class MasterCardDebit: DebitCard() {
    private var bonus: Int = 0

    override fun pay(amount: Int) {
        super.pay(amount)
        if (amount > 100) {
            bonus += amount / 100
            println("Начислено ${amount / 100} бонусов!")
        }
    }

    override fun getAll() {
        super.getAll()
        println("| Бонусы: $bonus")
    }
}