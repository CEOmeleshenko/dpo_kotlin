package kotlin_09_OOP2

class ProCardDebit: DebitCard() {
    override fun pay(amount: Int) {
        if (amount in 5000..balance) {
            val cashback = amount / 100 * 5
            println("Кэшбэк $cashback!")
            balance -= amount
            balance += cashback
        }
        else super.pay(amount)
    }
}