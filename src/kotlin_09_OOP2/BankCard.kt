package kotlin_09_OOP2

abstract class BankCard {
    protected abstract var balance: Int

    abstract fun deposit(amount: Int)
    abstract fun pay(amount: Int)

    abstract fun getBalance()
    abstract fun getAll()
}