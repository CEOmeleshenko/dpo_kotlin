package kotlin_02_HelloKotlin
// Первое домашнее задание
/* Работа с функциями print и println
*   и
*   комментариями */

fun main() {
    println("Привет! ")
    println("Меня зовут Андрей, мне 18.")
    println("Я являюсь студентом. ")
    print("Имею небольшой опыт в написании кода на таких языках как ")
    print("C#, ")
    print("Python, ")
    print("HTML, CSS, JS")
}