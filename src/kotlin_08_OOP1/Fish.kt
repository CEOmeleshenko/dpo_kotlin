package kotlin_08_OOP1

import kotlin.random.Random

class Fish(
    name: String,
    maxAge: Int,
    weight: Int,
    energy: Int) : Animal(name, maxAge, weight, energy)
{
    override fun move() {
        super.moveMethod = "плывет"
        super.move()
    }

    override fun birthOfChildren(): Fish {
        super.birthOfChildren()
        return Fish(this.name, this.maxAge, Random.nextInt(1, 10), Random.nextInt(1, 5))
    }

    override fun randomAction(): String {
        val actions = listOf("move", "eat", "sleep", "birthOfChildren")
        val action = actions.random()
        when (action) {
            "move" -> this.move()
            "eat" -> this.eat()
            "sleep" -> this.sleep()
            "birthOfChildren" -> return action
        }
        return action
    }
}