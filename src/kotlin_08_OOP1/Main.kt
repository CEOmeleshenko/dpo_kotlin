package kotlin_08_OOP1

fun main(){
    val natureReserve = NatureReserve()

    // Количество циклов
    val numOfIterator = 1

    val numOfAnimals = natureReserve.lifecycle(numOfIterator).size
    println("\nКоличество животных в заповеднике: $numOfAnimals")
}
