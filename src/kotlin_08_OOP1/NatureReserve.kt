package kotlin_08_OOP1

class NatureReserve {
    private var collectAnimals = mutableListOf(
        Bird("орел", 4, 1, 5),
        Bird("ястреб", 4, 1, 6),
        Bird("беркут", 3, 1, 5),
        Bird("голубь", 2, 1, 3),
        Bird("воробей", 5, 3, 8),
        Fish("тунец", 8, 4, 6),
        Fish("акула", 10, 8, 10),
        Fish("окунь", 4, 2, 5),
        Dog("корги", 12, 6, 10),
        Dog("шпиц", 16, 10, 10),
        Animal("медведь", 24, 30, 20),
        Animal("хомяк", 22, 25, 20)
    )
    private var numOfAnimals = collectAnimals.size

    fun lifecycle(numOfIterator: Int): List<Animal> {
        val newCollect: MutableList<Animal> = mutableListOf()
        newCollect.addAll(collectAnimals)

        for (i in 1..numOfIterator) {
            for (obj in newCollect) {
                if (obj.randomAction() == "birthOfChildren") {
                    numOfAnimals++
                    collectAnimals.add(obj.birthOfChildren())
                }
            }
            newCollect.clear()
            newCollect.addAll(collectAnimals)
            for (obj in newCollect) {
                if (obj.isTooOld || obj.energy <= 0) {
                    numOfAnimals--
                    collectAnimals.remove(obj)
                }
            }
            newCollect.clear()
            newCollect.addAll(collectAnimals)

            if (numOfAnimals <= 0) {
                println("В заповеднике не осталось животных")
                return newCollect
            }
        }
        return newCollect
    }
}