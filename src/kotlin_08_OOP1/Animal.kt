package kotlin_08_OOP1

import kotlin.random.Random

open class Animal(
    var name: String,
    var maxAge: Int,
    private var weight: Int,
    var energy: Int)
{
    private var age: Int = 1

    val isTooOld: Boolean = age >= maxAge

    var moveMethod: String = "передвигается"

    fun sleep() {
        energy += 5
        age += 1
        println("$name спит")
    }

    fun eat() {
        energy += 3
        weight++
        age++
        println("$name ест")
    }

    open fun move() {
        if (isTooOld && (energy - 5 <= 0 || weight - 1 <= 0)) return
        else {
            energy -= 5
            weight--
            age += if (tryIncrementAge()) 1 else 0
            println("$name $moveMethod")
        }
    }

    private fun tryIncrementAge(): Boolean = Random.nextBoolean()

    open fun birthOfChildren(): Animal {
        val childEnergy: Int = Random.nextInt(1, 10)
        val childWeight: Int = Random.nextInt(1, 5)
        println("У ${this.name} родился детёныш!")
        println("Характеристики животного:")
        println("   Максимальный возраст: ${this.maxAge} лет\n" +
                "   Энергия: $childEnergy\n" +
                "   Вес: $childWeight")
        return Animal(this.name, this.maxAge, childEnergy, childWeight)
    }

    open fun randomAction(): String {
        val actions = listOf("move", "eat", "sleep", "birthOfChildren")
        val action = actions.random()
        when (action) {
            "move" -> this.move()
            "eat" -> this.eat()
            "sleep" -> this.sleep()
            "birthOfChildren" -> return action
        }
        return action
    }
}