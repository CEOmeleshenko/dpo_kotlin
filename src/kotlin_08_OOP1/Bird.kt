package kotlin_08_OOP1

import kotlin.random.Random

class Bird(
    name: String,
    maxAge: Int,
    weight: Int,
    energy: Int) : Animal(name, maxAge, weight, energy)
{

    override fun move() {
        super.moveMethod = "летит"
        super.move()
    }

    override fun birthOfChildren(): Bird {
        super.birthOfChildren()
        return Bird(this.name, this.maxAge, Random.nextInt(1, 10), Random.nextInt(1, 5))
    }

    override fun randomAction(): String {
        val actions = listOf("move", "eat", "sleep", "birthOfChildren")
        val action = actions.random()
        when (action) {
            "move" -> this.move()
            "eat" -> this.eat()
            "sleep" -> this.sleep()
            "birthOfChildren" -> return action
        }
        return action
    }
}