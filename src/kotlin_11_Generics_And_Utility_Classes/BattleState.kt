package kotlin_11_Generics_And_Utility_Classes

sealed class BattleState {

    object Progress {
        fun getInfo(team1: Team, team2: Team) {
            println("= Количество воинов Первой команды: ${team1.team.stack.size}")
            println("= Количество воинов Второй команды: ${team2.team.stack.size}")
        }
    }

    object FirstTeamWon {
        fun getResult() {
            println("РЕЗУЛЬТАТ СРАЖЕНИЯ: ПОБЕДА ПЕРВОЙ КОМАНДЫ")
        }
    }

    object SecondTeamWon {
        fun getResult() {
            println("РЕЗУЛЬТАТ СРАЖЕНИЯ: ПОБЕДА ВТОРОЙ КОМАНДЫ")
        }
    }

    object Draw {
        fun getResult() {
            println("РЕЗУЛЬТАТ СРАЖЕНИЯ: НИЧЬЯ")
        }
    }
}
