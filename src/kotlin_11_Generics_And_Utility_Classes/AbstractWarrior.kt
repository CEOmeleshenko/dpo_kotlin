package kotlin_11_Generics_And_Utility_Classes

abstract class AbstractWarrior(
    maxHP: Int,
    override var chanceToDodge: Int,
    private var accuracy: Int,
    var weapon: AbstractWeapon
): Warrior {
    private var currentHP: Int = maxHP

    override fun attack(enemy: Warrior) {
        try {
            if (!weapon.isAmmoAvailability) {
                throw NoAmmoException()
            } else {
                print("Стреляю(${ when (weapon.fireType) {
                    is FireType.SingleShot -> "одиночный"
                    is FireType.MultiShot -> "очередь"
                }
                }): ")
                weapon.getAmmo().stack.forEach {
                    if (accuracy.chance()) {
                        if (enemy.chanceToDodge.chance()) print("ПРОМАХ ")
                        else {
                            val damage = it.getFullDamage()
                            enemy.getDamage(damage)
                            if (damage > weapon.createAmmo().damage) print("-$damage(крит.) ")
                            else print("-$damage ")
                        }
                    } else print("ПРОМАХ ")
                }
                println()
            }
        }
        catch (ex: NoAmmoException) {
            println("Магазин пустой, перезаряжаю... (ПРОПУСК ХОДА)")
            weapon.reload()
        }
    }
    override fun getDamage(damage: Int) {
        currentHP -= damage
        isKilled = currentHP <= 0
    }
}