package kotlin_11_Generics_And_Utility_Classes

class Soldier(
    maxHP: Int = 100,
    chanceToDodge: Int = 20,
    accuracy: Int = 50,
    weapon: AbstractWeapon = Weapons.createPistol()
) : AbstractWarrior(maxHP, chanceToDodge, accuracy, weapon) {
    override var isKilled: Boolean = false
}