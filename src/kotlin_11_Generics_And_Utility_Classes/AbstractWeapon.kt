package kotlin_11_Generics_And_Utility_Classes

abstract class AbstractWeapon(
    var maxNumOfAmmo: Int,
    var fireType: FireType,
    private var magazine: Stack<Ammo> = Stack(),
    var isAmmoAvailability: Boolean = false
) {
    abstract fun createAmmo(): Ammo

    fun reload() {
        for (i in magazine.stack.size + 1..maxNumOfAmmo) {
            magazine.push(createAmmo())
        }
        isAmmoAvailability = true
    }

    fun getAmmo(): Stack<Ammo> {
        val stackAmmo: Stack<Ammo> = Stack()
        try {
            when (fireType) {
                is FireType.SingleShot -> {
                    if (isAmmoAvailability) magazine.pop()?.let { stackAmmo.push(it) }
                    else throw NoAmmoException()
                }
                is FireType.MultiShot -> {
                    if (magazine.stack.size >= (fireType as FireType.MultiShot).length) {
                        for (i in 1..(fireType as FireType.MultiShot).length) {
                            magazine.pop()?.let { stackAmmo.push(it) }
                        }
                    }
                    else throw NoAmmoException()
                }
            }
        }
        finally {
            if (magazine.stack.isEmpty()) isAmmoAvailability = false
        }
        return stackAmmo
    }
}