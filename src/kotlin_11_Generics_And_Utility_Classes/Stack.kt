package kotlin_11_Generics_And_Utility_Classes

import kotlin.random.Random


open class Stack<T> {

    var stack: MutableList<T> = mutableListOf()

    fun push(item: T) = stack.add(item)

    fun pop(): T? {
        val item = stack.lastOrNull()
        if (!isEmpty()) {
            stack.remove(stack.last())
        }
        return item
    }

    fun random() : T? {
        val indexItem = Random.nextInt(0, stack.size)
        val item = stack[indexItem]
        if (!isEmpty()) {
            stack.remove(stack[indexItem])
        }
        return item
    }

    fun isEmpty(): Boolean {
        return stack.isEmpty()
    }
}
