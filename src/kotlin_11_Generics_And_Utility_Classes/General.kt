package kotlin_11_Generics_And_Utility_Classes

class General(
    maxHp: Int = 240,
    chanceToDodge: Int = 60,
    accuracy: Int = 80,
    weapon: AbstractWeapon = Weapons.createSniperRifle()
) : AbstractWarrior(maxHp, chanceToDodge, accuracy, weapon) {
    override var isKilled: Boolean = false
}