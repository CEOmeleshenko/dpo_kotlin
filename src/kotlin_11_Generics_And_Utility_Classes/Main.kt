package kotlin_11_Generics_And_Utility_Classes

import kotlin.random.Random

fun main() {
    val game = Battle()

    battleStart(game)

}

fun battleStart(game: Battle) {
    var round = 0
    while (!game.battleIsOver) {
        round++
        println("=== РАУНД $round")
        game.nextMove()
        println("=== РАУНД $round ЗАВЕРШЕН\n")
    }
    game.battleState()
}

fun Int.chance(): Boolean {
    return this >= Random.nextInt(100)
}
