package kotlin_11_Generics_And_Utility_Classes

enum class Ammo(
    val damage: Int,
    private val criticalDamageChance: Int,
    private val criticalDamageCoefficient: Int
) {
    BULLET(20, 15, 2),
    ARMOR_PIERCING_BULLET(50, 30, 2),
    INCENDIARY_BULLET(30, 50, 4);

    fun getFullDamage(): Int {
        return if (criticalDamageChance.chance()) {
            damage * criticalDamageCoefficient
        } else damage
    }
}