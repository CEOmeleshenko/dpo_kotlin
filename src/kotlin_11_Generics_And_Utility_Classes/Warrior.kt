package kotlin_11_Generics_And_Utility_Classes

interface Warrior {
    var isKilled: Boolean
    var chanceToDodge: Int

    fun attack(enemy: Warrior)
    fun getDamage(damage: Int)
}