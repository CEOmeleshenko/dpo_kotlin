package kotlin_11_Generics_And_Utility_Classes

object Weapons {
    fun createPistol(): AbstractWeapon {
        return object : AbstractWeapon(
            maxNumOfAmmo = 8,
            fireType = FireType.SingleShot
        ) {
            override fun createAmmo(): Ammo = Ammo.BULLET
        }
    }

    fun createAutomaticRifle() : AbstractWeapon {
        return object : AbstractWeapon(
            maxNumOfAmmo = 25,
            fireType = FireType.MultiShot(5)
        ) {
            override fun createAmmo(): Ammo = Ammo.BULLET
        }
    }

    fun createSniperRifle() : AbstractWeapon {
        return object : AbstractWeapon(
            maxNumOfAmmo = 6,
            fireType = FireType.SingleShot
        ) {
            override fun createAmmo(): Ammo = Ammo.ARMOR_PIERCING_BULLET
        }
    }

    fun createShotgun() : AbstractWeapon {
        return object : AbstractWeapon(
            maxNumOfAmmo = 2,
            fireType = FireType.SingleShot
        ) {
            override fun createAmmo(): Ammo = Ammo.INCENDIARY_BULLET
        }
    }
}