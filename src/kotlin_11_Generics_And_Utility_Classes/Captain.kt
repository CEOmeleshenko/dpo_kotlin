package kotlin_11_Generics_And_Utility_Classes

class Captain(
    maxHP: Int = 180,
    chanceToDodge: Int = 40,
    accuracy: Int = 60,
    weapon: AbstractWeapon = Weapons.createAutomaticRifle()
) : AbstractWarrior(maxHP, chanceToDodge, accuracy, weapon) {
    override var isKilled: Boolean = false
}