package kotlin_11_Generics_And_Utility_Classes

class NoAmmoException: Throwable(message = "Нет необходимого количества патронов для выстрела")