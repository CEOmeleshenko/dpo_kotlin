package kotlin_11_Generics_And_Utility_Classes

class Team {
    var team: Stack<AbstractWarrior> = Stack()

    fun fillTeam(numOfWarrior: Int) {
        for (i in 1..numOfWarrior) {
            if (10.chance()) team.push(General())
            else if (40.chance()) team.push(Captain())
            else team.push(Soldier())
        }
    }

    fun getMembers() {
        var generals = 0
        var capitans = 0
        var soldiers = 0
        for (member in team.stack) {
            when (member) {
                is General -> generals++
                is Captain -> capitans++
                is Soldier -> soldiers++
            }
        }
        print("Состав: $generals генералов, $capitans капитанов, $soldiers солдат.\n")
    }
}