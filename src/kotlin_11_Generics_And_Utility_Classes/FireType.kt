package kotlin_11_Generics_And_Utility_Classes

sealed class FireType {
    object SingleShot: FireType() { const val length: Int = 1}
    data class MultiShot(val length: Int) : FireType()
}

