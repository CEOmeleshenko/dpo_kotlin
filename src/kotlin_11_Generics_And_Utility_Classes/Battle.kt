package kotlin_11_Generics_And_Utility_Classes

class Battle {
    private var team1: Team = Team()
    private var team2: Team = Team()
    var battleIsOver: Boolean = false

    init {
        println("=== СИМУЛЯЦИЯ СРАЖЕНИЯ ===")
        print("Введите численность Первой команды -> ")
        team1.fillTeam(readln().toInt())
        print("Введите численность Второй команды -> ")
        team2.fillTeam(readln().toInt())

        print("Первая команда < - > ")
        team1.getMembers()
        print("Вторая команда < - > ")
        team2.getMembers()

        println()
    }

    fun battleState() {
        if (!battleIsOver) BattleState.Progress.getInfo(team1, team2)
        else {
            if (team1.team.isEmpty() && team2.team.isEmpty()) BattleState.Draw.getResult()
            else if (!team1.team.isEmpty() && team2.team.isEmpty()) BattleState.FirstTeamWon.getResult()
            else BattleState.SecondTeamWon.getResult()
        }
    }

    fun nextMove() {
        if (!battleIsOver) {
            val warrior1 = team1.team.random()
            val warrior2 = team2.team.random()

            println("(Первая команда)${getNameWarrior(warrior1)} ПРОТИВ (Вторая команда)${getNameWarrior(warrior2)}")

            if (warrior1 != null && warrior2 != null) {
                if (warrior1.weapon.isAmmoAvailability) {
                    println("= ХОД (Первая команда)${getNameWarrior(warrior1)}")
                    warrior1.attack(warrior2)
                    if (warrior2.isKilled) {
                        println("(Вторая команда)${getNameWarrior(warrior2)} убит")
                        team1.team.push(warrior1)
                    } else {
                        if (warrior2.weapon.isAmmoAvailability) {
                            println("= ХОД (Вторая команда)${getNameWarrior(warrior2)}")
                            warrior2.attack(warrior1)
                            if (warrior1.isKilled) {
                                println("(Первая команда)${getNameWarrior(warrior1)} убит")
                                team2.team.push(warrior2)
                            } else {
                                team1.team.push(warrior1)
                                team2.team.push(warrior2)
                            }
                        } else {
                            println("= ХОД (Вторая команда)${getNameWarrior(warrior2)}")
                            warrior2.attack(warrior1)
                            team1.team.push(warrior1)
                            team2.team.push(warrior2)
                        }
                    }
                } else {
                    println("= ХОД (Первая команда)${getNameWarrior(warrior1)}")
                    warrior1.attack(warrior2)
                    if (warrior2.weapon.isAmmoAvailability) {
                        println("= ХОД (Вторая команда)${getNameWarrior(warrior2)}")
                        warrior2.attack(warrior1)
                        if (warrior1.isKilled) {
                            println("(Первая команда)${getNameWarrior(warrior1)} убит")
                            team2.team.push(warrior2)
                        } else {
                            team1.team.push(warrior1)
                            team2.team.push(warrior2)
                        }
                    } else {
                        println("= ХОД (Вторая команда)${getNameWarrior(warrior2)}")
                        warrior2.attack(warrior1)
                        team1.team.push(warrior1)
                        team2.team.push(warrior2)

                    }
                }
                battleState()
                if (team1.team.isEmpty() || team2.team.isEmpty()) battleIsOver = true
            }
        }
    }

    private fun getNameWarrior(warrior: AbstractWarrior?) : String {
        when (warrior) {
            is Soldier -> return "Солдат"
            is Captain -> return "Капитан"
            is General -> return "Генерал"
        }
        return ""
    }
}