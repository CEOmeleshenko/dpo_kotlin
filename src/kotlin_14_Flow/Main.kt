package kotlin_14_Flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.takeWhile

fun main() {
    val player1 = Card("Андрей",1)
    val player2 = Card("Павел", 1)
    val player3 = Card("Игорь", 1)
    val generator = Generator

    val players = arrayListOf(player1, player2, player3)

    println("Начало игры в Лото!")
    runBlocking {
        players.forEach {
            launch {
                generator.numbers().takeWhile { generator.isGame }.collect { number ->
                    if (it.mark(number) == 15) {
                        println("${it.playerName} выиграл!")
                        generator.isGame = false
                    }
                }
            }
        }
        println("Игра окончена!")
    }
}

