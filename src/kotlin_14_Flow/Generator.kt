package kotlin_14_Flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

object Generator {
    private val numbers = (1..90).toList().shuffled()
    var isGame = true


    fun numbers(): Flow<Int> = flow {
        numbers.forEach {
            emit(it)
        }
    }
}