package kotlin_14_Flow

import kotlinx.coroutines.yield

class Card(val playerName: String, private val num: Int = 1) {
    private val numbers = Array(num) { Array(3) { Array(5) { 0 } } }
    private var markedNumbers = Array(num){ 0 }

    init {
        val temp = (1..90).toList().shuffled()
        var index = 0
        for (card in 0 until num) {
            for (row in 0 until 3) {
                for (num in 0 until 5) {
                    numbers[card][row][num] = temp[index++]
                }
            }
        }
        showCard()
    }

     suspend fun mark(number: Int) : Int {
        for (card in 0 until num) {
            for (row in 0 until 3) {
                for (num in 0 until numbers[card][row].size ) {
                    if (numbers[card][row][num] == number) {
                        println("$playerName: найдено число $number в карточке ${card + 1}")
                        markedNumbers[card]++
                    }
                }
            }
        }
        yield()
        return markedNumbers.maxOrNull() ?: 0
    }

    private fun showCard() {
        println("Поля игрока $playerName:")
        for (i in numbers) {
            for (j in i) {
                print("[ ")
                for (k in j) {
                    print("$k ")
                }
                print("]\n")
            }
        }
    }
}