package kotlin_07_ClassesAndObjects

class TV(private val mark: String, private val model: String, private val diagonal: Int, private val numOfChannels: Int) {
    var isON: Boolean = false
        private set

    private var currentVolume = 0

    private var channels: HashMap<Int, String> = Channels.getRandomChannels(numOfChannels)
    private var currentChannel: Int = 1

    // Инициализация
    init {
        println("Телевизор добавлен.")
    }

    // Информация о телевизоре
    fun tvInfo() {
        println("Телевизор: марка - $mark, модель - $model, диагональ - $diagonal")
        println("Количество каналов: ${channels.size}")
    }

    // Кнопка включения/выключения
    fun toggle() {
        if (isON) {
            isON = !isON
            println("Телевизор выключен")
        }
        else {
            isON = !isON
            println("Телевизор включен")
            println("Текущий канал: $currentChannel - ${channels[currentChannel]}. Текущая громкость: $currentVolume/$maxVolume.")
        }
    }

    // Увеличить громкость на value
    fun incVolume(value: Int) {
        if (!isON) toggle()
        currentVolume = if (currentVolume + value > maxVolume) maxVolume else currentVolume + value
        println("Громкость увеличена на $value. Текущая громкость: $currentVolume/$maxVolume")
    }

    // Уменьшить громкость на value
    fun decVolume(value: Int) {
        if (!isON) toggle()
        currentVolume = if (currentVolume - value < 0) 0 else currentVolume - value
        println("Громкость уменьшена на $value. Текущая громкость: $currentVolume/$maxVolume")
    }

    // Переключить канала на заданный
    fun switchChannel(number: Int) {
        if (!isON) toggle()
        if (number <= channels.size) {
            currentChannel = number
            println("Канал переключен на $number - ${channels[number]}")
        }
        else {
            currentChannel = channels.size - number
            println("Канал переключен на ${channels.size - number} - ${channels[channels.size - number]}")
        }
    }

    // Переключить канал вперед
    fun switchChannelUp() {
        if (!isON) toggle()
        if (currentChannel + 1 > channels.size) {
            currentChannel = 1
            println("Нажата кнопка вверх. Канал переключен на $currentChannel - ${channels[currentChannel]}")
        }
        else {
            currentChannel += 1
            println("Нажата кнопка вверх. Канал переключен на $currentChannel - ${channels[currentChannel]}")
        }
    }

    // Переключить канал назад
    fun switchChannelDown() {
        if (!isON) toggle()
        if (currentChannel -1 <= 0) {
            currentChannel = channels.size
            println("Нажата кнопка вниз. Канал переключен на $currentChannel - ${channels[currentChannel]}")
        }
        else {
            currentChannel -= 1
            println("Нажата кнопка вниз. Канал переключен на $currentChannel - ${channels[currentChannel]}")
        }
    }

    // Вывод списка каналов
    fun channelsInfo() {
        println("Список каналов:")
        for (channel in channels.entries) {
            println("    " + "${channel.key} - ${channel.value}")
        }
    }

    companion object {
        const val maxVolume = 100
    }
}