package kotlin_07_ClassesAndObjects

object Channels {
    private var channels: List<String> = listOf(
        "News",
        "Sport",
        "Music",
        "Movies",
        "Science",
        "For kids",
        "Auto",
        "Fun",
        "Cooking",
        "Travel"
        )

    fun getRandomChannels(number: Int): HashMap<Int, String> {
        var num = number
        if (num > channels.size) num = channels.size
        val listChannels: HashMap<Int, String> = hashMapOf()
        val tempList = channels.shuffled()
        for (i in 1..num){
            listChannels[i] = tempList[i - 1]
        }
        return listChannels
    }
}