package kotlin_07_ClassesAndObjects

fun main() {
    val tv1 = TV("Samsung", "F2", 24, 5)
    tv1.tvInfo()
    println(tv1.isON)
    tv1.toggle()
    tv1.channelsInfo()
    println()
    Thread.sleep(1000)

    val tv2 = TV("LG", "A0D22", 12, 8)
    tv2.tvInfo()
    tv2.channelsInfo()
    tv2.switchChannel(4)
    tv2.incVolume(80)
    tv2.decVolume(100)
    println()
    Thread.sleep(1000)

    val tv3 = TV("Sony", "XX232", 26, 12)
    tv3.tvInfo()
    tv3.channelsInfo()
    for (i in 0..3) {
        tv3.switchChannelDown()
    }
    tv3.toggle()
    tv3.switchChannelUp()
    println()
    Thread.sleep(1000)
}