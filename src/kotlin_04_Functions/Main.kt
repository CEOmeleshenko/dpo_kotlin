package kotlin_04_Functions

fun main() {
    val str = "F2p)v\"y233{0->c}ttelciFc"

    val strTranscript = part1Transcript(str) + part2Transcript(str)
    println(strTranscript)
}

fun part1Transcript(str: String) : String {
    var part1 = str.dropLast(str.length / 2)
    part1 = shift(part1, -1)
    part1 = part1.replace('5', 's')
    part1 = part1.replace('4', 'u')
    part1 = shift(part1, 3)
    part1 = part1.replace('0', 'o')
    return part1
}

fun part2Transcript(str: String) : String {
    var part2 = str.drop(str.length / 2)
    part2 = part2.reversed()
    part2 = shift(part2, 4)
    part2 = part2.replace('_', ' ')
    return part2
}

fun shift(str: String, shift: Int): String {
    return str.map { char -> char - shift }.joinToString("")
}