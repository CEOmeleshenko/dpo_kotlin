package kotlin_06_Collections

import kotlin.random.Random

fun main() {
    val n = readNum()

    var list = List(n){ Random.nextInt(-5, 5)}
    println("Новый список: $list")

    list = list.toMutableList()
    for (i in list.indices) {
        if (i % 2 != 0) list[i] *= 10
    }
    println("Измененный список: $list")

    println("Сумма элементов списка: " + list.sumOf { it })

    println("Положительные числа списка: " + list.filter { it > 0 })
}

fun readNum() : Int {
    while (true) {
        print("Введите число больше 0 -> ")
        val num: Int = readLine()?.toIntOrNull() ?: continue
        if (num > 0) return num
        else continue
    }
}