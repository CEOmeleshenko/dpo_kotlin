package kotlin_06_Collections

fun main(){
    print("Введите число пользователей для регистрации -> ")
    val num: Int = readLine()?.toIntOrNull() ?: 1
    val dataBase = registration(num)
    println("$dataBase" + "\n")

    authorization(dataBase)
}

fun registration(num: Int): HashMap<String, String> {
    val dataBase = hashMapOf<String, String>()
    println("Подготовка данных начата...")
    for (i in 1..num){
        print("Заполните логин для $i пользователя: ")
        var login: String
        while (true) {
            login = readLine().toString()
            if (login == "" || login in dataBase) {
                print("Введите другое значение: ")
            continue }
            else break
        }

        print("Заполните пароль для $i пользователя: ")
        var password: String
        while (true) {
            password = readLine().toString()
            if (password == "") {
                print("Введите другое значение: ")
                continue }
            else break
        }
        dataBase[login] = password
    }
    println("Подготовка данных закончена!")
    return dataBase
}

fun authorization(dataBase: HashMap<String, String>) {
    println("Авторизация пользователей:")
    while (true) {
        print("Введите логин: ")
        val login = readLine().toString()
        if (dataBase.keys.contains(login)) {
            print("Введите пароль: ")
            val password = readLine().toString()
            if (dataBase.getValue(login) == password) println("Добро пожаловать $login")
            else {
                println("Указан неверный пароль\n")
                continue
            }
        } else {
            println("Пользователь не найден\n")
            continue
        }
        break
    }
}